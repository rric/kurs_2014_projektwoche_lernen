/** Example 3: TODO Add a proper program name.
    TODO Describe the program in one sentence.
    TODO Your name(s) here.
*/


void setup()
{
    size(800, 500);
    background(0, 0, 0);
}


void drawSmiley(int mX, int mY)
{
    int centerX = width / 2; // Coordinates of the screen center
    int centerY = height / 2;

    // Draw the yellow face, moved a little bit
    int posX = centerX + 2 * (mX - centerX);
    int posY = centerY + 2 * (mY - centerY);
    fill(255, 224, 0);
    ellipse(posX, posY, 55, 55);

    // Draw the two black eyes
    fill(0, 0, 0);
    ellipse(posX - 10, posY - 8, 11, 11);
    ellipse(posX + 10, posY - 8, 11, 11);

    // Draw the smiling lips
    float fromAngle = PI / 5.0;
    float toAngle   = 4.0 * fromAngle;
    noFill();
    arc(posX, posY + 8, 25, 15, fromAngle, toAngle);
}


void draw()
{
    // TODO What happens here?
    drawSmiley(mouseX, mouseY);
}

