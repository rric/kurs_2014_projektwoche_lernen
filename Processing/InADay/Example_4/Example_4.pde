/** Example 4: TODO Add a proper program name.
    TODO Describe the program in one sentence.
    TODO Your name(s) here.
*/


void setup()
{
    size(800, 500);
    background(0, 0, 0);
}


// TODO What is returned by this function?
int getPosX(int mX)
{
    int centerX = width / 2;
    return constrain(centerX + 2 * (mX - centerX), 30, width - 30);
}


int getPosY(int mY)
{
    int centerY = height / 2;
    return constrain(centerY + 2 * (mY - centerY), 30, height - 30);
}


void drawSmiley(int mX, int mY)
{
    // Draw the yellow face, moved a little bit
    int posX = getPosX(mX);
    int posY = getPosY(mY);
    fill(255, 224, 0);
    ellipse(posX, posY, 55, 55);

    // Draw the two black eyes
    fill(0, 0, 0);
    ellipse(posX - 10, posY - 8, 11, 11);
    ellipse(posX + 10, posY - 8, 11, 11);

    // Draw the smiling lips
    float fromAngle = PI / 5.0;
    float toAngle   = 4.0 * fromAngle;
    noFill();
    arc(posX, posY + 8, 25, 15, fromAngle, toAngle);
}


void draw()
{
    // Draw a smiley face, moving with the mouse.
    drawSmiley(mouseX, mouseY);
}


