/** Example 7: TODO Add a proper program name.
    TODO Describe the program in one sentence.
    TODO Your name(s) here.
*/

// TODO What does this list contain?
ArrayList<Vector2d> smileys = new ArrayList<Vector2d>();


void setup()
{
    size(800, 500);
    background(0, 0, 0);

    // Lets add some random friends
    smileys.add(new Vector2d(random(-50, 50), random(-50, 50)));
    smileys.add(new Vector2d(random(-50, 50), random(-50, 50)));
    smileys.add(new Vector2d(random(-50, 50), random(-50, 50)));
    smileys.add(new Vector2d(random(-50, 50), random(-50, 50)));
}


// Returns the position of the smiley's face,
// depending on the position m.
Vector2d getPos(Vector2d m)
{
    Vector2d center = new Vector2d(width / 2, height / 2);

    Vector2d pos = add(center, mult(2, sub(m, center)));

    // Ensure that position is within screen
    if (pos.x < 0) {
        pos.x = -pos.x;
    } else if (pos.x > width) {
        pos.x = width - (pos.x - width);
    }

    if (pos.y <= 0) {
        pos.y = -pos.y;
    } else if (pos.y >= height) {
        pos.y = height - (pos.y - height);
    }

    return pos;
}


void drawSmiley(Vector2d m)
{
    // Draw the yellow face, moved a little bit
    Vector2d pos = getPos(m);
    fill(255, 224, 0);
    ellipse(pos.x, pos.y, 55, 55);

    // Draw the two black eyes
    fill(0, 0, 0);
    ellipse(pos.x - 10, pos.y - 8, 11, 11);
    ellipse(pos.x + 10, pos.y - 8, 11, 11);

    // Draw the smiling lips
    float fromAngle = PI / 5.0;
    float toAngle   = 4.0 * fromAngle;
    noFill();
    arc(pos.x, pos.y + 8, 25, 15, fromAngle, toAngle);
}


void draw()
{
    // Reset the background to black
    background(0, 0, 0);

    // Draw several smiley faces, moving with the mouse.
    Vector2d m = new Vector2d(mouseX, mouseY);

    drawSmiley(add(m, smileys.get(0)));
    drawSmiley(add(m, smileys.get(1)));
    drawSmiley(add(m, smileys.get(2)));
    drawSmiley(add(m, smileys.get(3)));
}

