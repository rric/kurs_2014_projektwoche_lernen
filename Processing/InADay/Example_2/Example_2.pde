/** Example 2: TODO Add a proper program name.
    TODO Describe the program in one sentence.
    TODO Your name(s) here.
*/


void setup()
{
    size(800, 500);
    background(0, 0, 0);
}


void draw()
{
    int posX = mouseX;     // Let the current mouse position be
    int posY = mouseY;     // the center of the drawing

    // TODO What happens here?
    fill(255, 224, 0);
    ellipse(posX, posY, 55, 55);

    // TODO What happens here?
    fill(0, 0, 0);
    ellipse(posX - 10, posY - 8, 11, 11);
    ellipse(posX + 10, posY - 8, 11, 11);

    // Draw the smiling lips
    float fromAngle = PI / 5.0;
    float toAngle   = 4.0 * fromAngle;
    noFill();
    arc(posX, posY + 8, 25, 15, fromAngle, toAngle);
}

