/** Example 6: TODO Add a proper program name.
    TODO Describe the program in one sentence.
    TODO Your name(s) here.
*/


void setup()
{
    size(800, 500);
    background(0, 0, 0);
}


// Returns the position of the smiley's face,
// depending on the position m.
Vector2d getPos(Vector2d m)
{
    Vector2d center = new Vector2d(width / 2, height / 2);

    Vector2d pos = add(center, mult(2, sub(m, center)));

    // TODO What happens here?
    if (pos.x < 0) {
        pos.x = -pos.x;
    } else if (pos.x > width) {
        pos.x = width - (pos.x - width);
    }

    if (pos.y < 0) {
        pos.y = -pos.y;
    } else if (pos.y > height) {
        pos.y = height - (pos.y - height);
    }

    return pos;
}


void drawSmiley(Vector2d m)
{
    // Draw the yellow face, moved a little bit
    Vector2d pos = getPos(m);
    fill(255, 224, 0);
    ellipse(pos.x, pos.y, 55, 55);

    // Draw the two black eyes
    fill(0, 0, 0);
    ellipse(pos.x - 10, pos.y - 8, 11, 11);
    ellipse(pos.x + 10, pos.y - 8, 11, 11);

    // Draw the smiling lips
    float fromAngle = PI / 5.0;
    float toAngle   = 4.0 * fromAngle;
    noFill();
    arc(pos.x, pos.y + 8, 25, 15, fromAngle, toAngle);
}


void draw()
{
    // Draw a smiley face, moving with the mouse.
    drawSmiley(new Vector2d(mouseX, mouseY));
}

