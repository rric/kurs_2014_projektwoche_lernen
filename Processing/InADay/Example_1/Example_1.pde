/** Example 1: TODO Add a proper program name.
    TODO Describe the program in one sentence.
    TODO Your name(s) here.
*/


// setup() is executed once when the program starts.
void setup()
{
    size(800, 500);        // Set display size
    background(0, 0, 0);   // TODO What happens here?
}


// draw() is executed in a loop until the program is stopped.
void draw()
{
    fill(255, 224, 0);               // Use yellow as fill color
    ellipse(mouseX, mouseY, 55, 55); // Draw circle at current mouse position
}

