/* MottBotKNN.ino
 *
 * Copyright 2013 Roland Richter.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Metro.h>
#include <PVector.h>

/* ----------------------------------------------------------------------------
 * Layout for "small" bots:
 */ 

/* The TB6612FNG controls a motor via three pins: two for direction, one for
 * speed. Additionally, there is a Standby pin to turn off/on the whole board.
 */
 
#include <TB6612FNGMotor.h>

uint8_t LeftPins[]  = {9, 8, 7, 6};    // Standby, Dir1, Dir2, PWM
uint8_t RightPins[] = {9, 10, 11, 12}; // Standby, Dir1, Dir2, PWM

TB6612FNGMotor leftMotor(LeftPins);
TB6612FNGMotor rightMotor(RightPins);

// Brightness is detected by two photo resistors.
const int LeftPhoto  = A1;
const int RightPhoto = A0;

// The joystick is read via two analog axis inputs, and one digital push button.
const int HorizAxis    = A5;
const int VertAxis     = A4;
const int SelectButton = 5;

// A pushbutton to change the interal mode.
const int ModeButton = 3;

// LED pin to indicate joystick select button:
const int LEDPin = 13;


/* ----------------------------------------------------------------------------
 * Layout for "large" bot:
 */
 
/*
 * The ArduMoto controls a motor via two pins: one for direction, one for speed.
 */

/*
#include <ArduMotoR.h>

uint8_t LeftPins[]  = {12, 3};  // Dir, PWM
uint8_t RightPins[] = {13, 11}; // Dir, PWM

ArduMotoR leftMotor(LeftPins);
ArduMotoR rightMotor(RightPins);

// Brightness is detected by two photo resistors.
const int LeftPhoto  = A5;
const int RightPhoto = A4;

// The joystick is read via two analog axis inputs, and one digital push button.
const int HorizAxis    = A0;
const int VertAxis     = A1;
const int SelectButton = 2;

// A pushbutton to change the interal mode.
const int ModeButton = 5;

// LED pin to indicate joystick select button:
const int LEDPin = 7;
*/

// ----------------------------------------------------------------------------

// The two sides of the bot are not necessarily symmetric, so "going straight
// forward" might be not that "straight". To compensate for that, one may vary
// the speed of the left and right motors, resp.
// TODO Calibrate these values automatically?

const int MaxLeftSpeed  = 128;
const int MaxRightSpeed = 128;

boolean lastModeState = HIGH;

enum {Learn, Recall} mode = Learn;


// Timer to determine when to read sensors.
Metro readTick(500);

// Rather than defining Joystick state like this
// enum JoystickState {None, Pressed, Forward, Backward, Left, Right};
// which causes lots of trouble when used as a return type, lets do this:

const byte Undefined     = 0;
const byte MoveNot      = 1;
const byte MoveForward  = 2; 
const byte MoveBackward = 3; 
const byte MoveLeft     = 4; 
const byte MoveRight    = 5;

int readAxis(int axisPin) 
{ 
    int reading = map(analogRead(axisPin), 0, 1023, -255, 255);

    if (abs(reading) < 128) {
      reading = 0;
    } 
  
    return reading;
}


byte readJoystickState(int selectButtonPin, int horizAxisPin, int vertAxisPin)
{
    boolean select = digitalRead(selectButtonPin);
    int horizAxis = readAxis(horizAxisPin);            
    int vertAxis  = readAxis(vertAxisPin);
    
    byte joystick = Undefined;
    
    // The select button is used with the internal pullup resistor, 
    // so it is LOW when pressed, and HIGH otherwise.
    if (select == LOW) {
        joystick = MoveNot;
    } else {
        
        if (vertAxis == 0 && horizAxis == 0) {
            joystick = Undefined;
        }
        // The joystick is mounted such that V points backwards,
        // and H points to the right.
        else if (abs(vertAxis) >= abs(horizAxis)) {
            if (vertAxis < 0) {
                joystick = MoveForward;
            } else if (vertAxis > 0) {
                joystick = MoveBackward;
            }
        } else {
            if (horizAxis < 0) {
                joystick = MoveLeft;
            } else if (horizAxis > 0) {
                joystick = MoveRight;
            }
        }
    }
    
    return joystick;
}


int nSamples = 0;

PVector sample[30];
byte    label[30];


void setup() 
{
    Serial.begin(9600);

    pinMode(SelectButton, INPUT_PULLUP);
    pinMode(ModeButton, INPUT_PULLUP);
    
    pinMode(LEDPin, OUTPUT);
    
    leftMotor.stop();
    rightMotor.stop();
}


void loop()
{
    if (readTick.check()) {
        // Switch between Learn and Recall mode if mode button was pressed before, 
        // and is released now. The mode button is used with the internal pullup
        // resistor, so it is LOW when pressed, and HIGH otherwise.
        boolean modeState = digitalRead(ModeButton);
        if (lastModeState == LOW && modeState == HIGH) {
            mode = (mode == Learn ? Recall : Learn);
        }
        lastModeState = modeState;
        
        readTick.interval(mode == Learn ? 500 : 100);
        
        // Read left and right brightness, and map them to [0,100].
        int leftBrightness = analogRead(LeftPhoto);
        int rightBrightness = analogRead(RightPhoto);
        
        byte l = map(leftBrightness,  0, 1023, 0, 100);
        byte r = map(rightBrightness, 0, 1023, 0, 100);
        
        Serial.print("l: ");
        Serial.print(l);
        Serial.print(", r: ");
        Serial.println(r);
        
        // Stop in both modes.
        leftMotor.stop();
        rightMotor.stop();
        
        // In Learn mode, ...
        if (mode == Learn) { 
            digitalWrite(LEDPin, LOW);
            byte movement = readJoystickState(SelectButton, HorizAxis, VertAxis);

            if (movement != Undefined) {
                sample[nSamples] = PVector(l, r, 0);
                label[nSamples]  = movement;
                
                nSamples = (nSamples + 1) % 30;
                
                digitalWrite(LEDPin, HIGH);
                delay(100);
                digitalWrite(LEDPin, LOW);
            }
        } 
        // In Recall mode, ...
        else {
            digitalWrite(LEDPin, HIGH);
            PVector current(l, r, 0);  
            
            float bestDist = 10000.0;
            byte  bestMove = Undefined;
            
            for (int i = 0; i < nSamples; ++i) {
                float dist = PVector::dist(sample[i], current);
                
                if (dist < bestDist) {
                    bestDist = dist; 
                    bestMove = label[i];  
                }
            }
            
            // Depending on the best movement, either 
            // move forwards, backwards, left, or right.
            int leftSpeed  = 0;
            int rightSpeed = 0;
            
            switch (bestMove) {
                case MoveForward:
                    leftSpeed  = +MaxLeftSpeed;
                    rightSpeed = +MaxRightSpeed;
                    break;
                case MoveBackward:
                    leftSpeed  = -MaxLeftSpeed;
                    rightSpeed = -MaxRightSpeed;
                    break;
                case MoveLeft:
                    leftSpeed  = -MaxLeftSpeed;
                    rightSpeed = +MaxRightSpeed;
                    break;
                case MoveRight:
                    leftSpeed  = +MaxLeftSpeed;
                    rightSpeed = -MaxRightSpeed;
                    break;
                case Undefined: case MoveNot: default:
                    break;
            }
        
            if (leftSpeed != 0 || rightSpeed != 0) {
                leftMotor.start(leftSpeed);
                rightMotor.start(rightSpeed);
            }
        }
    }
}

