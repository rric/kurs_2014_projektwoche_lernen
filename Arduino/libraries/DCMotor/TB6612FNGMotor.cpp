/* TB6612FNGMotor.cpp
 *
 * Copyright 2013 Roland Richter.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TB6612FNGMotor.h"

/* The TB6612FNG controls a motor via three pins: two for direction, one for
 * speed. Additionally, there is a Standby pin to turn off/on the whole board.
 * Hence, the constructor expects four pins: Standby, Dir1, Dir2, PWM
 */	
TB6612FNGMotor::TB6612FNGMotor(uint8_t pins[4])
{
	init(pins);	
}


// Sets pinMode of the given pins to OUTPUT.
void TB6612FNGMotor::init(uint8_t pins[4]) 
{
	for (uint8_t i = 0; i < 4; ++i) {
		_pins[i] = pins[i];
		pinMode(_pins[i], OUTPUT);
	}
}


// Starts the motor with the given speed, which might be in [-255,255].
void TB6612FNGMotor::start(int speed) {
    digitalWrite(_pins[0], HIGH);

    boolean dir1 = (speed >= 0 ? LOW : HIGH);
    boolean dir2 = (speed >= 0 ? HIGH : LOW);

    digitalWrite(_pins[1], dir1);
    digitalWrite(_pins[2], dir2);
    analogWrite(_pins[3], abs(speed));
}


// Stops the motor.
void TB6612FNGMotor::stop() {
	digitalWrite(_pins[0], LOW);
}
