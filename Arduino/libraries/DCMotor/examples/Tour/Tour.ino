/* Tour.ino
 *
 * Copyright 2012 Johannes Kepler Universitaet Linz,
 * Institut fuer Wissensbasierte Mathematische Systeme.
 *
 * Copyright 2013 Roland Richter.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
/* A small sketch to test one of several motor shields.
 * Comment and uncomment code below as needed.
 */


/* -----------------------------------------------------------------------------
 * ArduMoto shield, see http://www.sparkfun.com/products/9815
 * For instructions how to hook up the driver and the motors,
 * see https://www.sparkfun.com/tutorials/195
 * The ArduMoto controls a motor via two pins: one for direction, one for speed.
 */


#include <ArduMotoR.h>

uint8_t LeftPins[]  = {12, 3};  // Dir, PWM
uint8_t RightPins[] = {13, 11}; // Dir, PWM

ArduMotoR leftMotor(LeftPins);
ArduMotoR rightMotor(RightPins);



/* -----------------------------------------------------------------------------
 * TB6612FNG Motor Driver, see http://www.sparkfun.com/products/9457
 * For instructions how to hook up the driver and the motors,
 * see http://bildr.org/2012/04/tb6612fng-arduino/
 * The TB6612FNG controls a motor via three pins: two for direction, one for
 * speed. Additionally, there is a Standby pin to turn off/on the whole board.
 */

/*
#include <TB6612FNGMotor.h>

uint8_t LeftPins[]  = {9, 8, 7, 6};    // Standby, Dir1, Dir2, PWM
uint8_t RightPins[] = {9, 10, 11, 12}; // Standby, Dir1, Dir2, PWM

TB6612FNGMotor leftMotor(LeftPins);
TB6612FNGMotor rightMotor(RightPins);
*/

void setup()
{
    leftMotor.stop();
    rightMotor.stop();
}


void loop()
{
    // Full stop.
    leftMotor.stop();
    rightMotor.stop();

    delay(3000);

    // Forward at full speed.
    leftMotor.start(+255);
    rightMotor.start(+255);

    delay(2000);

    // Rotate clockwise.
    leftMotor.start(+255);
    rightMotor.start(-255);

    delay(2000);  
   
    // Rotate counter-clockwise.
    leftMotor.start(-255);
    rightMotor.start(+255);

    delay(2000);  
   
    // Backward at full speed.
    leftMotor.start(-255);
    rightMotor.start(-255);

    delay(2000);
}

