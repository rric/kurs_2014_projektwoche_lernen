/* ArduMotoR.cpp
 *
 * Copyright 2012 Johannes Kepler Universitaet Linz,
 * Institut fuer Wissensbasierte Mathematische Systeme.
 *
 * Copyright 2013 Roland Richter.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ArduMotoR.h"


/* The ArduMoto controls a motor via two pins: one for direction, one
 * for speed. Hence, the constructor expects two pins: Dir, PWM
 */	
ArduMotoR::ArduMotoR(uint8_t pins[2])
{
	init(pins);	
}


// Sets pinMode of the given pins to OUTPUT.
void ArduMotoR::init(uint8_t pins[2]) 
{
	for (uint8_t i = 0; i < 2; ++i) {
		_pins[i] = pins[i];
		pinMode(_pins[i], OUTPUT);
	}
}


// Starts the motor with the given speed, which might be in [-255,255].
void ArduMotoR::start(int speed) {
    digitalWrite(_pins[0], (speed >= 0 ? LOW : HIGH));  
    analogWrite(_pins[1], abs(speed));
}


// Stops the motor.
void ArduMotoR::stop() {
    digitalWrite(_pins[0], LOW);  
    analogWrite(_pins[1], 0);
}
