/* TB6612FNGMotor.h
 *
 * Copyright 2013 Roland Richter.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TB6612FNGMOTOR_H
#define TB6612FNGMOTOR_H

#include <Arduino.h>

/* Class to control a DC motor via the TB6612FNG board.
 */
class TB6612FNGMotor
{
public:
	/* The TB6612FNG controls a motor via three pins: two for direction, one for
	 * speed. Additionally, there is a Standby pin to turn off/on the whole board.
	 * Hence, the constructor expects four pins: Standby, Dir1, Dir2, PWM
	 */	
	TB6612FNGMotor(uint8_t pins[4]);

	// Sets pinMode of the given pins to OUTPUT.
	void init(uint8_t pins[4]);

	// Starts the motor with the given speed, which might be in [-255,255].
	void start(int speed);

	// Stops the motor.
	void stop();

private:
	uint8_t _pins[4];
};

#endif
