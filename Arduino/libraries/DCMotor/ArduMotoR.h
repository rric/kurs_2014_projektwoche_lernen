/* ArduMotoR.h
 *
 * Copyright 2012 Johannes Kepler Universitaet Linz,
 * Institut fuer Wissensbasierte Mathematische Systeme.
 *
 * Copyright 2013 Roland Richter.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARDUMOTOR_H
#define ARDUMOTOR_H

#include <Arduino.h>

/* Class to control a DC motor via the ArduMoto shield.
 */
class ArduMotoR
{
public:
	/* The ArduMoto controls a motor via two pins: one for direction, one
	 * for speed. Hence, the constructor expects two pins: Dir, PWM
	 */	
	ArduMotoR(uint8_t pins[2]);

	// Sets pinMode of the given pins to OUTPUT.
	void init(uint8_t pins[2]);

	// Starts the motor with the given speed, which might be in [-255,255].
	void start(int speed);

	// Stops the motor.
	void stop();

private:
	uint8_t _pins[2];
};

#endif
