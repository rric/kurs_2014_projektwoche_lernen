/* BotPerceptron.ino
 *
 * Copyright 2013, 2014 Roland Richter.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Metro.h>
#include <PVector.h>

// The Perceptron class.
class Perceptron
{
    // The separating line which is to be found by the Perceptron algorithm
    // is given by the normal vector w (weights) and the offset b (bias).
    // The expression
    //      w.x * u.x + w.y * u.y + b
    // should be positive for all u in Ps, and negative for u in Ns.
    PVector w;
    float b;
    float eps;

public:

    Perceptron(float epsilon) {
        eps = epsilon;
    }

    void setWeights(PVector newW) {
        w = newW;
    }

    PVector weights() {
        return w;
    }

    void setBias(float newB) {
        b = newB;
    }

    float bias() {
        return b;
    }


    // Evaluates point u w.r.t. the Percetrons weights and bias.
    float evaluate(PVector u) {
        return PVector::dot(w, u) + b;
    }
    
    
    void normalizeTo(float newMag) {
        float mag = w.mag();
 
        if (mag > 0) {
            w = PVector::mult(w, newMag/mag);
            b = (newMag/mag) * b; 
        }
    }
    
    
    float delta(PVector u, float label) {
        return -label * evaluate(u);
    }
    
    
    boolean update(PVector u, float label) {
        // The two error cases
        // label == -1 && w . u + b >= 0 (an N was wrongly classified as P), and
        // label == +1 && w . u + b <= 0 (an P was wrongly classified as N)
        // can be summarized as
        float delta = -label * evaluate(u);
        // which is positive if and only if the classification is wrong.

        if (delta >= 0) {
            // The Perceptron is often formulated with the update rule
            // w += label * u;
            // b += label;
            // which would be equivalent to set our lambda value to
            // float lambda = 1.;
            // However, one might also choose other values for lambda.
            // The following choice (for any eps being positive),
            float lambda = (delta + eps) / (u.x * u.x + u.y * u.y + 1);
            // has the property that, after updating, w . u + b is _exactly_ eps.

            w.x += label * lambda * u.x;
            w.y += label * lambda * u.y;
            b += label * lambda;
            
            normalizeTo(10.);

            return true;
        } else {
            return false;
        }
    }    
};

Perceptron perceptron(10.);

float tolerance = 0.;


/* ----------------------------------------------------------------------------
 * Layout for "small" bots:
 */ 

/* The TB6612FNG controls a motor via three pins: two for direction, one for
 * speed. Additionally, there is a Standby pin to turn off/on the whole board.
 */

#include <TB6612FNGMotor.h>

uint8_t LeftPins[]  = {9, 8, 7, 6};    // Standby, Dir1, Dir2, PWM
uint8_t RightPins[] = {9, 10, 11, 12}; // Standby, Dir1, Dir2, PWM

TB6612FNGMotor leftMotor(LeftPins);
TB6612FNGMotor rightMotor(RightPins);

// Brightness is detected by two photo resistors.
const int LeftPhoto  = A1;
const int RightPhoto = A0;

// The joystick is read via two analog axis inputs, and one digital push button.
const int HorizAxis    = A5;
const int VertAxis     = A4;
const int SelectButton = 5;
boolean invertedHorizOrient = false;

// A pushbutton to change the interal mode.
const int ModeButton = 3;

// LED pin to indicate joystick select button:
const int LEDPin = 13;


/* ----------------------------------------------------------------------------
 * Layout for "large" bot:
 */
 
/*
 * The ArduMoto controls a motor via two pins: one for direction, one for speed.
 */
/*
#include <ArduMotoR.h>

uint8_t LeftPins[]  = {12, 3};  // Dir, PWM
uint8_t RightPins[] = {13, 11}; // Dir, PWM

ArduMotoR leftMotor(LeftPins);
ArduMotoR rightMotor(RightPins);

// Brightness is detected by two photo resistors.
const int LeftPhoto  = A5;
const int RightPhoto = A4;

// The joystick is read via two analog axis inputs, and one digital push button.
const int HorizAxis    = A0;
const int VertAxis     = A1;
const int SelectButton = 2;
boolean invertedHorizOrient = true;

// A pushbutton to change the interal mode.
const int ModeButton = 5;

// LED pin to indicate joystick select button:
const int LEDPin = 7;
*/

// ----------------------------------------------------------------------------

// The two sides of the bot are not necessarily symmetric, so "going straight
// forward" might be not that "straight". To compensate for that, one may vary
// the speed of the left and right motors, resp.
// TODO Calibrate these values automatically?

const int MaxLeftSpeed  = 128;
const int MaxRightSpeed = 128;

boolean lastModeState = HIGH;

enum {Learn, Recall} mode = Learn;

// Timer to determine when to read sensors.
Metro readTick(100);


// Rather than defining Joystick state like this
// enum JoystickState {None, Pressed, Forward, Backward, Left, Right};
// which causes lots of trouble when used as a return type, lets do this:

const byte Undefined    = 0;
const byte MoveNot      = 1;
const byte MoveForward  = 2; 
const byte MoveBackward = 3; 
const byte MoveLeft     = 4; 
const byte MoveRight    = 5;

int readAxis(int axisPin) 
{ 
    int reading = map(analogRead(axisPin), 0, 1023, -255, 255);

    if (abs(reading) < 128) {
      reading = 0;
    } 
  
    return reading;
}


byte readJoystickState(int selectButtonPin, int horizAxisPin, int vertAxisPin)
{
    boolean select = digitalRead(selectButtonPin);
    int horizAxis = readAxis(horizAxisPin);            
    int vertAxis  = readAxis(vertAxisPin);
    
    byte joystick = Undefined;
    
    // The select button is used with the internal pullup resistor, 
    // so it is LOW when pressed, and HIGH otherwise.
    if (select == LOW) {
        joystick = MoveNot;
    } else {        
        if (vertAxis == 0 && horizAxis == 0) {
            joystick = Undefined;
        }
        // The joystick is mounted such that V points backwards,
        // and H points to the right.
        else if (abs(vertAxis) >= abs(horizAxis)) {
            if (vertAxis < 0) {
                joystick = MoveForward;
            } else if (vertAxis > 0) {
                joystick = MoveBackward;
            }
        } else {
            // Sometimes left and right are switched
            if (horizAxis < 0) {
                joystick = (invertedHorizOrient ? MoveRight : MoveLeft);
            } else if (horizAxis > 0) {
                joystick = (invertedHorizOrient ? MoveLeft : MoveRight);
            }
        }
    }
    
    return joystick;
}


void setup() 
{
    Serial.begin(9600);

    pinMode(SelectButton, INPUT_PULLUP);
    pinMode(ModeButton, INPUT_PULLUP);
    
    pinMode(LEDPin, OUTPUT);
    
    leftMotor.stop();
    rightMotor.stop();

    perceptron.setWeights(PVector(0., 0.));
    perceptron.setBias(0.);
}


void printValue(char* name, float value) 
{
    Serial.print(name);
    Serial.print(value);
}

void printPVector(char* name, PVector v)
{
    Serial.print(name);
    Serial.print(v.x);
    Serial.print(",");
    Serial.print(v.y);
}

void printPerceptron()
{
    printPVector(" w: ", perceptron.weights());
    printValue(" b: ", perceptron.bias());
}


void blinkLED()
{
     digitalWrite(LEDPin, HIGH);
     delay(100);
     digitalWrite(LEDPin, LOW);   
}


void loop()
{
    if (readTick.check()) {
        // Switch between Learn and Recall mode if mode button was pressed before,
        // and is released now. The mode button is used with the internal pullup 
        // resistor, so it is LOW when pressed, and HIGH otherwise.
        boolean modeState = digitalRead(ModeButton);
        if (lastModeState == LOW && modeState == HIGH) {
            mode = (mode == Learn ? Recall : Learn);
        }
        lastModeState = modeState;
        
        // Read left and right brightness, and map them to [0,100].
        int leftBrightness = analogRead(LeftPhoto);
        int rightBrightness = analogRead(RightPhoto);
        
        byte l = map(leftBrightness,  0, 1023, 0, 100);
        byte r = map(rightBrightness, 0, 1023, 0, 100);

        // Stop in both modes.
        leftMotor.stop();
        rightMotor.stop();

        // The current sample point.
        PVector u((float)l, (float)r);
        float eval = perceptron.evaluate(u);
        
        if (mode == Learn) { 
            digitalWrite(LEDPin, LOW);
            byte movement = readJoystickState(SelectButton, HorizAxis, VertAxis);
            
            // Depending on the joystick state, only show info,
            // or learn with label -1 or +1.
            
            float delta;

            switch (movement) {
                case MoveNot: 
                    printPVector(" u: ", u);
                    printPerceptron();
                    printValue(" eval: ", eval);
                    Serial.println("");
                    break;
                case MoveForward:
                    printPVector(" u: ", u);
                    if (tolerance < abs(eval)) {
                        tolerance = abs(eval);
                        printValue(" => tol: ", tolerance);
                    } else {
                        printValue(" tol: ", tolerance);
                    }
                    Serial.println("");                    
                    break;
                case MoveBackward:
                    break;
                case MoveLeft: 
                    printPVector(" u: ", u);
                    delta = perceptron.delta(u, -1.);
                    printValue(" delta: ", delta);
                    if (perceptron.update(u, -1.)) {
                        Serial.print(" => ");
                        printPerceptron();
                        blinkLED();
                    }
                    Serial.println("");
                    break;
                case MoveRight:
                    printPVector(" u: ", u);
                    delta = perceptron.delta(u, +1.);
                    printValue(" delta: ", delta);
                    if (perceptron.update(u, +1.)) {
                        Serial.print(" => ");
                        printPerceptron();
                        blinkLED();
                    }
                    Serial.println("");
                    break;
                case Undefined: default:
                    break;
            }
        } 
        else if (mode == Recall) { 
            digitalWrite(LEDPin, HIGH);
            
            byte bestMove = Undefined;
            
            eval = perceptron.evaluate(u);
            
            if (eval < -tolerance) {
                bestMove = MoveLeft;
            } else if (eval > +tolerance) {
                bestMove = MoveRight;   
            } else {
                bestMove = MoveForward;
            }
            
            // Depending on the best movement, either 
            // move forwards, backwards, left, or right.
            int leftSpeed  = 0;
            int rightSpeed = 0;
            
            switch (bestMove) {
                case MoveForward:
                    leftSpeed  = +MaxLeftSpeed;
                    rightSpeed = +MaxRightSpeed;
                    break;
                case MoveBackward:
                    leftSpeed  = -MaxLeftSpeed;
                    rightSpeed = -MaxRightSpeed;
                    break;
                case MoveLeft:
                    leftSpeed  = -MaxLeftSpeed;
                    rightSpeed = +MaxRightSpeed;
                    break;
                case MoveRight:
                    leftSpeed  = +MaxLeftSpeed;
                    rightSpeed = -MaxRightSpeed;
                    break;
                case Undefined: case MoveNot: default:
                    break;
            }
        
            if (leftSpeed != 0 || rightSpeed != 0) {
                leftMotor.start(leftSpeed);
                rightMotor.start(rightSpeed);
            }
        }
    }
}

